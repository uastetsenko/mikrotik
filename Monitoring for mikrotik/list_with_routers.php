<?php
$mysqli = new mysqli("localhost", "name_of_user", "}M1234@,123I", "name_of_base");
$mysqli->query("SET NAMES 'utf8'");

$list = array();
$sumdw = 0;
$sumul = 0;

$good = $mysqli->query("SELECT * FROM `routers`");

while($row = mysqli_fetch_array($good)){
    $list[] =$row;
    $sumdw = $row['dw'] + $sumdw;
    $sumul = $row['ul'] + $sumul;
}
$mysqli->close ();

?>	

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <title>Наши роутеры</title>
        <link rel="stylesheet" href="styles.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="top_box">
            <div class="first_box">
                <div class="col_list">Количество роутеров: <span><?=count($list)?></span> шт</div>
                <div class="sum_dw">Все скачали: <span><?=$sumdw?></span> ГБ</div>
                <div class="sum_ul">Все загрузили: <span><?=$sumul?></span> ГБ</div>
            </div>
            <div class="sec_box">
                <select class="select_box" onchange="location = this.value;">
                <option selected value="#TI">TI</option>
                <option value="#270">Борисполь, ул. Киевская 123</option>
                <option value="#302">Винница, ул. Рогулева 12</option>

                </select>
            </div>
        </div>
    <div class="wrapper">
        <?php foreach ($list as $key){?>
        <div class="box_with_router" id="<?=$key['code']?>">
            <div class="box_head">
                <div class="r_code"><i class="fa fa-address-card" aria-hidden="true"> <?=$key['code']?></i></div>
                <div class="r_name"><?=$key['name']?> <i class="fa fa-location-arrow" aria-hidden="true"></i></div>
            </div>
            <table class="box_table">
                <tr>
                    <td>Модель</td>
                    <td><?=$key['model']?></td>
                </tr>
                <tr>
                    <td>IP</td>
                    <td><?=$key['ip']?></td>
                </tr>

                <tr>
                    <td>Время работы</td>
                    <td><?=$key['uptime']?></td>
                </tr>
                <tr>
                    <td>Нагрузка ЦП</td>
                    <td>
                    <?php
                        if (strval($key['cpu']) >= 50){
                            print("<div class='box_info red'>".$key['cpu']." %</div>");
                        } else {
                            print("<div class='box_info green'>".$key['cpu']." %</div>"); 
                        }
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Занятое место</td>
                    <td>
                    <?php
                        $vals = intval($key['memtotal']) - intval($key['memfree']);
                        if ($vals <= 5){
                            print("<div class='box_info red'>".$key['memfree']." / ".$key['memtotal']." МВ</div>");
                        } else {
                            print("<div class='box_info green'>".$key['memfree']." / ".$key['memtotal']." МВ</div>"); 
                        }
                    ?>    
                    </td>
                </tr>
                <tr>
                    <td>Текущая версия</td>
                    <td><?=$key['version']?></td>
                </tr>
                <tr>
                    <td>Версия обновления</td>
                    <td><?=$key['uversion']?></td>
                </tr>
                <tr>
                    <td>Последний ответ</td>
                    <td><?php
                    $datenow = date("Y-m-d H:i:s");
                    $dd = $key['date'];
                    $d2 = strtotime("now") - strtotime($dd);
                    $mins = floor($d2 / 60);

                    if ($mins >= 0 and $mins <= 1) {
                        print("<div class='box_info green'>Пару секунд назад</div>"); 
                    }
                    elseif ($mins >= 1 and $mins <= 4){
                        print("<div class='box_info green'>".$mins." минуты назад</div>");       
                    }
                    elseif ($mins >= 1 and $mins <= 60){
                        print("<div class='box_info green'>".$mins." минут назад</div>");       
                    } else {
                        $hours = floor($mins / 60);
                        if ($hours == 1 or $hours == 21){
                            print("<div class='box_info red'>".$hours." час назад</div>");
                        } elseif ($hours >= 2 and $hours <= 4){
                            print("<div class='box_info red'>".$hours." часа назад</div>");
                        } elseif ($hours >= 5 and $hours <= 20){
                            print("<div class='box_info red'>".$hours." часов назад</div>");
                        } elseif ($hours >= 22 and $hours <= 24){
                            print("<div class='box_info red'>".$hours." часа назад</div>");
                        } else {
                           print($key['date']); 
                        }
                    }
                    ?></td>
                </tr>
            </table>

            <div class="r_info">
                <?php 
                    $version = preg_replace("/[^0-9.]/", '', $key['version']);
                    $uversion = preg_replace("/[^0-9.]/", '', $key['uversion']);
                    if ($version != $uversion){ ?>
                        <div class="box_info red stl">Пора обновиться</div>
                    <?} else {?>
                        <div class="box_info green stl">Обновлений нет</div>
                    <?}?>
            </div>

            <div class="box_footer">
                <div class="r_dw"><i class="fa fa-download" aria-hidden="true"> <?=$key['dw']?> GB</i></div>
                <div class="r_ul">GB <?=$key['ul']?> <i class="fa fa-upload" aria-hidden="true"></i></div>
            </div> 
        </div>
    <? } ?>
    </div>
    </body>
</html>