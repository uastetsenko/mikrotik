/interface wireless security-profiles
add authentication-types=wpa2-psk eap-methods="" management-protection=\
    allowed mode=dynamic-keys name=TT supplicant-identity="" \
    wpa-pre-shared-key=123456789 wpa2-pre-shared-key=123456789
add authentication-types=wpa2-psk eap-methods="" management-protection=\
    allowed mode=dynamic-keys name=other supplicant-identity="" \
    wpa-pre-shared-key=privetnewwifi wpa2-pre-shared-key=privetnewwifi
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-XX \
    country=ukraine disabled=no distance=indoors frequency=2422 mode=\
    ap-bridge security-profile=TT ssid=W-TT wireless-protocol=802.11 \
    wps-mode=disabled
add disabled=no keepalive-frames=disabled mac-address=66:D1:54:BA:48:8A \
    master-interface=wlan1 multicast-buffering=disabled name=wlan2 \
    security-profile=TT ssid=O-TT wds-cost-range=0 wds-default-cost=0 \
    wps-mode=disabled

/ip address
set 0 address=10.10.30.1/24 network=10.10.30.0
add address=10.10.99.1/24 interface=wlan2 network=10.10.99.0

/ip pool
set 0 ranges=10.10.30.10-10.10.30.20
add name=other-wifi ranges=10.10.99.100-10.10.99.200

/ip dhcp-server network
set 0 address=10.10.30.0/24 gateway=10.10.30.1
add address=10.10.99.0/24 gateway=10.10.99.1

/ip dhcp-server
add add-arp=yes address-pool=other-wifi disabled=no interface=wlan2 name=other-wifi

/system clock
set time-zone-name=Europe/Kiev

/ip firewall filter
add action=accept chain=input src-address=77.143.143.70
add action=accept chain=input dst-port=9100 in-interface=ether1 protocol=tcp
set numbers=[find action=fasttrack-connection] disabled=yes
move numbers=[find src-address="77.143.143.70"] destination=3
move numbers=[find dst-port="9100"] destination=3

/ip firewall nat
add action=dst-nat chain=dstnat dst-port=9100 in-interface=ether1 protocol=\
    tcp to-addresses=10.10.30.99 to-ports=9100

/ip route rule
add action=unreachable dst-address=10.10.99.0/24 src-address=10.10.30.0/24
add action=unreachable dst-address=10.10.30.0/24 src-address=10.10.99.0/24

/ip service
set telnet disabled=yes
set ftp disabled=yes
set www disabled=yes
set ssh disabled=yes
set winbox address=77.143.143.70/32,148.111.187.180/32,10.10.30.0/24 port=33389

/system clock
set time-zone-name=Europe/Kiev

/user
add group=full name=odmin password=ParolSuper
remove admin

/system identity
set name=NewName