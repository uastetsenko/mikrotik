/ip firewall layer7-protocol
add name=low_sites regexp="^.+(youtube.com|www.youtube.com|m.youtube.com|ytimg\
    .com|s.ytimg.com|ytimg.l.google.com|youtube.l.google.com|goog\
    levideo.com|youtu.be|facebook.com|instagram.com|vk.com|twitter.com|tumblr.\
    com|pinterest.com|habr.com|livejournal.com|linkedin.com|ok.ru|korresponden\
    t.net|unian.net|censor.net.ua|apostrophe.ua|segodnya.ua|obozrevatel.com|li\
    ga.net|ua-ix.biz|tsn.ua|24tv.ua|gordonua.com|lenta.ru|rbc.ua|strana.ua|112\
    .ua|gazeta.ua|ukranews.com|44.ua|kinokrad.co|ivi.ru|kino50.net|kinogo-2018\
    .net|smotri-filmi.cc|filmix.co|hdrezka.ag|kinotochka.club|megogo.net|kinog\
    o.cc|baskino.me|ex-fs.net|ex-fs.com|videomore.ru|kinoprofi.org|seasonvar.r\
    u|rosserial.net|baskino1.top|skfilms.tv|datalock.ru|mastarti.com|radio.i.u\
    a|tochka.net|rusradio.ua|onlineradiobox.com|russiafm.net|radio-online.com.\
    ua|kissfm.ua).*\$"
add name=block_anonim regexp="^.+(anonim.pro|anonimizing.com|hideme.ru|wbprx.c\
    om|www.seogadget.ru/anonymizer|zawq.ru|2ip.ru/anonim/|proxy.aboutip.de|pin\
    gway.ru|eastfeed.ru|cameleo.ru|nblu.ru|westfeed.ru|unlumen.ru|anonymizer.c\
    om.ua|ru.xnet.club|jinkl.ru|kalarupa.com|www.dkproxy.com|www.proxy-2014.co\
    m|pinun.ru|blaim.ru|anonymizer.ru|harrachov.ru|noblock.ru|spoolls.com|paxo\
    ne.ru|finestats.ru|filestats.ru|paxtwo.ru|dostyp.ru|accesstheweb.info|ipv6\
    to4.com|www.theuniqueproxy.com|swisswebproxy.ch|unblockinstagram.com|www.h\
    ideandgo.com|mrchameleon.ru|dd34.ru|hidemyass.com|webvpn.org|anonim.com.ua\
    |webmasta.org|nezayti.ru|terli.ru|nezayti.ru|vkzam.ru|qutor.ru|vkaa.ru|oke\
    st.ru|okclass.ru|okzahodi.ru|golovavkrovi.ru|loktivkrovi.ru|anonimno.biz|w\
    ix.com|dostupest.ru|recker.ru|fastbuh.ru|aradero.ru|vkdor.ru|forfolks.ru|s\
    erqus.ru|arendadorogo.ru|fantomik.ru|soldens.ru|sdamnedorogo.ru|faramond.r\
    u|parakeds.ru|webmurk.ru|nogivkrovi.ru|molokonaloktyah.ru|molokonaglazah.r\
    u|glazvkrovi.ru|urlbl.ru|anonim.in.ua).*\$"
add name=good_sites regexp="^.+(kyivstar.ua|vodafone.ua|lifecell.ua|sam\
    sung.com|support.ua|tpd.com.ua|tf-ua.com|astelit.ukr|globalpay.com.ua|live\
    .com|ukr.net|smallpdf.com|novaposhta.ua|nors.sint.ua|accent-service.com.ua\
    |drive.google.com|telegram.org|alfabank.com.ua).*\$"
/ip firewall mangle
add action=mark-connection chain=prerouting comment="Good Sites" \
    layer7-protocol=good_sites new-connection-mark=good_sites-con \
    passthrough=yes
add action=mark-packet chain=forward connection-mark=good_sites-con \
    in-interface=ether1 new-packet-mark=good_sites-in passthrough=yes
add action=mark-packet chain=forward connection-mark=good_sites-con \
    new-packet-mark=good_sites-out out-interface=ether1 passthrough=yes
add action=mark-connection chain=prerouting comment=TT dst-port=3389 \
    new-connection-mark=tt passthrough=yes protocol=tcp
add action=mark-packet chain=forward connection-mark=tt in-interface=ether1 \
    new-packet-mark=tt-in passthrough=yes
add action=mark-packet chain=forward connection-mark=tt new-packet-mark=\
    tt-out out-interface=ether1 passthrough=yes
add action=mark-connection chain=prerouting comment=WEB dst-port=80,443,8080 \
    new-connection-mark=web passthrough=yes protocol=tcp
add action=mark-packet chain=forward connection-mark=web in-interface=ether1 \
    new-packet-mark=web-in passthrough=yes
add action=mark-packet chain=forward connection-mark=web new-packet-mark=\
    web-out out-interface=ether1 passthrough=yes
add action=mark-connection chain=prerouting comment=DVR dst-port=554,8000 \
    new-connection-mark=DVR passthrough=yes protocol=tcp
add action=mark-packet chain=forward connection-mark=DVR in-interface=ether1 \
    new-packet-mark=DVR-in passthrough=yes
add action=mark-packet chain=forward connection-mark=DVR new-packet-mark=\
    DVR-out out-interface=ether1 passthrough=yes
add action=mark-connection chain=prerouting comment=LOW layer7-protocol=\
    low_sites new-connection-mark=low_sites-con passthrough=yes
add action=mark-packet chain=forward connection-mark=low_sites-con \
    in-interface=ether1 new-packet-mark=low_sites-in passthrough=yes
add action=mark-packet chain=forward connection-mark=low_sites-con \
    new-packet-mark=low_sites-out out-interface=ether1 passthrough=yes
add action=mark-connection chain=prerouting comment="Blocked Anonim" \
    layer7-protocol=block_anonim new-connection-mark=block_anonim-con \
    passthrough=yes
add action=mark-packet chain=forward connection-mark=block_anonim-con \
    in-interface=ether1 new-packet-mark=block_anonim-in passthrough=yes
add action=mark-packet chain=forward connection-mark=block_anonim-con \
    new-packet-mark=block_anonim-out out-interface=ether1 passthrough=yes
add action=mark-connection chain=prerouting comment=NON connection-mark=\
    no-mark new-connection-mark=no-mark-con passthrough=yes
add action=mark-packet chain=forward connection-mark=no-mark-con \
    in-interface=ether1 new-packet-mark=no-mark-in passthrough=yes
add action=mark-packet chain=forward connection-mark=no-mark-con \
    new-packet-mark=no-mark-out out-interface=ether1 passthrough=yes
/queue tree
add max-limit=10M name=IN parent=global
add max-limit=10M name=OUT parent=global
add max-limit=5M name=web-in packet-mark=web-in parent=IN priority=3
add max-limit=5M name=web-out packet-mark=web-out parent=OUT priority=3
add max-limit=256k name=low_sites-in packet-mark=low_sites-in parent=IN \
    priority=6
add max-limit=256k name=low_sites-out packet-mark=low_sites-out parent=OUT \
    priority=6
add max-limit=5M name=no-mark-in packet-mark=no-mark-in parent=IN priority=5
add max-limit=5M name=no-mark-out packet-mark=no-mark-out parent=OUT \
    priority=5
add max-limit=64k name=blocked_anonim-in packet-mark=block_anonim-in parent=\
    IN priority=7
add max-limit=64k name=blocked_anonim-out packet-mark=block_anonim-out \
    parent=OUT priority=7
add limit-at=2M max-limit=10M name=good_sites-in packet-mark=good_sites-in \
    parent=IN priority=2
add limit-at=2M max-limit=10M name=good_sites-out packet-mark=good_sites-out \
    parent=OUT priority=2
add limit-at=2M max-limit=5M name=dvr-in packet-mark=DVR-in parent=IN \
    priority=4
add limit-at=2M max-limit=5M name=dvr-out packet-mark=DVR-out parent=OUT \
    priority=4
add limit-at=2M max-limit=10M name=TT-in packet-mark=tt-in parent=IN \
    priority=1
add limit-at=2M max-limit=10M name=TT-out packet-mark=DVR-out parent=OUT \
    priority=1